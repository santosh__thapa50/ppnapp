/************Global functions (dependencies : jQuery)*************/

function userTimezoneOffsetValue() {
    var arr = [];
    for (var i = 1; i <= 12; i++) {
        var d = new Date();
        // d.setDate(i);
        d.setMonth(i);
        newoffset = d.getTimezoneOffset();
        arr.push(newoffset);
    }
    //DST = Math.min.apply(null, arr);
    nonDST = Math.max.apply(null, arr);
    return (nonDST * (-1));
};

window.global = {
    //currentStateUrl: window.location.href,
    //previousStateUrl: '',
    //stateChangeDetect: function () {
    //        if (this.previousStateUrl == window.location.href) {
    //            this.previousStateUrl = this.currentStateUrl;
    //            this.currentStateUrl = window.location.href;
    //            console.log('back ');
    //            $('body').removeClass('affix');
    //            $('.slider-overlay').css({ 'display': 'none' });
    //        } else {
    //            this.previousStateUrl = this.currentStateUrl;
    //            this.currentStateUrl = window.location.href;
    //            console.log('front ');

    //        }
    //},
    logErrorsToBackend: function (errMsg) {
        return false;
        var errObj = {
            UserId: localStorage['_UserId'],
            ErrorMessage: errMsg
        };
        $.post(baseURL + '/ErrorPage/LogErrorToServer', errObj).success(function (data) { }).error(function (data, status) { });
        //$.post(apiURL + '/Values/LogOutError', errObj).success(function (data) { }).error(function (data, status) { });
    },

    isIOSDevice: function () {
        if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
            return true;
        } else {
            return false;
        }
    },

    getQueryStringValue: function (key) {
        return unescape(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + escape(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
    },

    getQueryParameterByName: function (name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    },

    parseError: function (o) {
        var err = [];
        for (var key in o.ModelState) {
            for (var i = 0; i < o.ModelState[key].length; i++) {
                err.push(o.ModelState[key][i]);
            }
        }
        return err;
    },

    activateAccount: function (acc) {

        $.get(apiURL + "/Register/ActivateAccount/" + acc)
            .success(function (r) { return; })
            .error(function (r) { return; });
    },

    getMatchedValueById: function (arr, id) {

        var obj = $.grep(arr, function (o) { return o.Id == id; });

        if (obj != null && obj.length > 0) { return obj[0].Value; }
        else { return null; }

    },

    getMatchedPropertyById: function (arr, id, prop) {

        var obj = $.grep(arr, function (o) { return o.Id == id; });

        if (obj != null && obj.length > 0) {
            if (prop !== null && prop !== undefined) { return obj[0][prop]; }
            else { return obj[0]; }
        } else { return null; }
    },

    getObjectMatchById: function (arr, id) {
        var obj = $.grep(arr, function (o) { return o.Id == id });
        if (obj != null && obj.length > 0) {
            return obj[0];
        }
        else {
            return null;
        }
    },

    getMatchedPropertyByProperty: function (arr, arrProp, matchValue, retProp) {
        try {
            var obj = $.grep(arr, function (o) { return o[arrProp] == matchValue; });
            if (obj != null && obj.length > 0) {
                if (retProp !== undefined && retProp !== null) { return obj[0][retProp]; }
                else { return obj[0]; }
            } else { return null; }
        } catch (e) {
            //console.log(e);
            //console.log(arguments.callee.caller);
            return null;
        }
    },

    getMatchedListByProperty: function (arr, arrProp, matchValue) {
        var objList = $.grep(arr, function (o) { return o[arrProp] == matchValue; });
        if (objList != null && objList.length > 0) {
            return objList;
        } else { return null; }
    },

    getMatchedIndexByObject: function (arr, arrProp, matchValue) {
        var index = -1;
        for (var i = 0; i < arr.length; i++) {
            if (arr[i][arrProp] == matchValue) {
                index = i;
                break;
            }
        }
        return index;
    },

    getMatchedCountByProperty: function (arr, arrProp, matchValue) {
        var objList = $.grep(arr, function (o) { return o[arrProp] == matchValue; });
        if (objList != null) {
            return objList.length;
        } else { return 0; }
    },

    getServerDatetime: function () {
        $.get(apiURL + '/Values/GetCurrentDate').success(function (data) { return data; });
    },

    getCurrentYear: function (scope, prop) {
        $.get(apiURL + '/Values/GetCurrentDate').success(function (data) {
            scope[prop] = new Date(data).getFullYear();
            scope.$apply();
        });
    },

    convertTextLinkToHtmlLinks: function (text) {
        var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
        var text1 = text.replace(exp, "<a href='$1' target='_blank'>$1</a>");
        var exp2 = /(^|[^\/])(www\.[\S]+(\b|$))/gim;
        return text1.replace(exp2, '$1<a target="_blank" href="http://$2">$2</a>');
    },

    isTokenValid: function () {
        $.get(apiURL + '/Values/GetCurrentDate')
            .success(function (data) {
                if (moment(data) < moment(localStorage._TokenExpires)) {
                    return true;
                }
                else {
                    return false;
                }
            })
            .error(function (data) {
                return false;
            });
    },

    requestRefreshToken: true,

    renewToken: function () {
        //logout if refresh token is not available
        if (!localStorage._RefreshToken) {
            if (localStorage['_ProfilePhoto']) { localStorage.removeItem('_ProfilePhoto'); }
            global.clearLocalStorage();
            if (location.pathname == '/account/' || location.pathname == 'Web/Home' || location.pathname == '/' || location.pathname == '') {
                location.reload();
            }
            else {
                location.assign(baseURL + '/account/#/login');
            }
            return;
        }

        var params = 'grant_type=refresh_token&client_id=&refresh_token=' + localStorage._RefreshToken;

        $.post(tokenURL, params)
            .success(function (data) {
                localStorage._Token = data.access_token;
                localStorage._RefreshToken = data.refresh_token;
                localStorage._TokenExpiresIn = data['expires_in'];
                localStorage._TokenIssued = data['.issued'];
                localStorage._TokenExpires = data['.expires'];

                setTimeout(function () {
                    location.reload();
                }, 500);

            })
            .error(function (data) {
                // temporary code to log token issue
                try {
                    if (localStorage['_ProfilePhoto']) { localStorage.removeItem('_ProfilePhoto'); }
                    if (localStorage['_Roles']) { localStorage.removeItem('_Roles'); }
                    if (localStorage['_ProfileImage']) { localStorage.removeItem('_ProfileImage'); }
                    if (localStorage['_OrgLogo']) { localStorage.removeItem('_OrgLogo'); }
                    if (localStorage['_PlayerSports']) { localStorage.removeItem('_PlayerSports'); }
                } catch (e) {
                    console.log(e);
                }
                var errMsg = "URL:" + params + " |||| " + JSON.stringify(localStorage) + " |||| " + JSON.stringify(data);
                global.logErrorsToBackend(errMsg);
                global.clearLocalStorage();
                if (location.pathname == '/account/' || location.pathname == 'Web/Home' || location.pathname == '/' || location.pathname == '') {
                    location.reload();
                }
                else {
                    location.assign(baseURL + '/account/#/login');
                }
            });
    },

    clearLocalStorage: function () {

        // alphabetically ascending order
        var items = [
            '_AccountCreatedDate',
            '_CurrentRoleName',
            '_DaysLeft',
            '_EditionCatId', '_EditionId', '_Email', '_EventGuid', '_ExternalProviders',
            '_FirstName',
            '_GoverningOrg',
            '_IsAdmin', '_IsEmailVerified', '_IsProfileFilled', '_IsSuperUser',
            '_JoinToEventMetaId',
            '_LastName', '_LicenseType',
            '_NotificationUserName',
            '_OrgBrandImg', '_OrgId', '_OrgLogo', '_OrgLogoPublic', '_OrgSeasonId', '_OrgSportsId', '_OrgSportsName', '_OrgTimezone', '_OrgTypeLookupName',
            '_PlayerSports', '_ProfileImage', '_Packages',
            '_RefreshToken', '_Roles',
            '_SelectedSportsId', '_SelectedSportsName',
            '_TempEmail', '_TempUsername', '_Token', '_TokenExpires', '_TokenExpiresIn', '_TokenIssued',
            '_UserEmail', '_UserId', '_UserType', '_Username',
            '_SelectedChlidName', '_SelectedChlidUserId', '_ChildList', '_SportsList', '_EventTypeList'
        ];

        for (var i = 0; i < items.length; i++) {
            localStorage.removeItem(items[i]);
        }
    },

    docCookies: {
        getItem: function (sKey) {
            if (!sKey) { return null; }
            return decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + encodeURIComponent(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null;
        },
        setItem: function (sKey, sValue, vEnd, sPath, sDomain, bSecure) {
            if (!sKey || /^(?:expires|max\-age|path|domain|secure)$/i.test(sKey)) { return false; }
            var sExpires = "";
            if (vEnd) {
                switch (vEnd.constructor) {
                    case Number:
                        sExpires = vEnd === Infinity ? "; expires=Fri, 31 Dec 9999 23:59:59 GMT" : "; max-age=" + vEnd;
                        break;
                    case String:
                        sExpires = "; expires=" + vEnd;
                        break;
                    case Date:
                        sExpires = "; expires=" + vEnd.toUTCString();
                        break;
                }
            }
            document.cookie = encodeURIComponent(sKey) + "=" + encodeURIComponent(sValue) + sExpires + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "") + (bSecure ? "; secure" : "");
            return true;
        },
        removeItem: function (sKey, sPath, sDomain) {
            if (!this.hasItem(sKey)) { return false; }
            document.cookie = encodeURIComponent(sKey) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT" + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "");
            return true;
        },
        hasItem: function (sKey) {
            if (!sKey) { return false; }
            return (new RegExp("(?:^|;\\s*)" + encodeURIComponent(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=")).test(document.cookie);
        },
        keys: function () {
            var aKeys = document.cookie.replace(/((?:^|\s*;)[^\=]+)(?=;|$)|^\s*|\s*(?:\=[^;]*)?(?:\1|$)/g, "").split(/\s*(?:\=[^;]*)?;\s*/);
            for (var nLen = aKeys.length, nIdx = 0; nIdx < nLen; nIdx++) { aKeys[nIdx] = decodeURIComponent(aKeys[nIdx]); }
            return aKeys;
        }
    },

    isValidEmail: function (email) {
        var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        return re.test(email);
    },

    isLocalStorageSupported: function () {
        try {
            window.localStorage.setItem("_isLocalStorageSupported", "true");
            window.localStorage.removeItem("_isLocalStorageSupported");
            return true;
        } catch (error) {
            return false;
        }
    },

    getArrayNotInArray: function (array, notInArray) {
        var resultArray = [];
        for (var i = 0; i < array.length; i++) {
            if ($.inArray(array[i], notInArray) === -1) {
                resultArray.push(array[i]);
            }
        }
        return resultArray;
    },

    // userTimezoneOffset: (new Date().getTimezoneOffset() * (-1)),

    userTimezoneOffset: (userTimezoneOffsetValue()),

    timeZoneWithGMTValue: function () {
        var d = new Date();
        var timeStr = d.toTimeString();
        var timeStrLst = timeStr.split(" ");
        return timeStrLst[1];
    },

    getTooltipText: function (array, property) {
        var obj = { Tooltip: '', ShortText: '' };
        if (array !== null && array.length > 0) {
            for (var i = 0; i < array.length; i++) {
                obj.Tooltip += array[i][property];
                if (i < 2) {
                    obj.ShortText += array[i][property];
                }
                if ((i + 1) < array.length) {
                    obj.Tooltip += ', ';
                    if (i < 1) {
                        obj.ShortText += ', ';
                    }
                }
            }
            if (array.length > 2) {
                obj.ShortText += '...';
            }
        }
        return obj;
    },

    isNullOrUndefined: function (model) {
        if (model === undefined || model === null) {
            return true;
        }
        else {
            return false;
        }
    },

    replaceNullOrUndefined: function (model, replaceBy) {
        if (model === undefined || model === null) {
            model = replaceBy;
        }
        return model;
    },

    addItemIsCheckedAndReturnList: function (arry) {
        if (arry && arry.length > 0) {
            for (var i = 0; i < arry.length; i++) {
                arry[i].IsChecked = false;
            }
        }
        return arry;
    },

    // Manipulation for new multiselect plugin
    comapreListWithMapperAndReturnList: function (lookupArray, selectedArray, mapper) {

        if (!mapper) {
            mapper = { Id: 'Id', Value: 'Value' };
        }

        if (lookupArray && lookupArray.length > 0) {

            var lenA = selectedArray.length;
            var lenB = lookupArray.length;

            for (var i = 0; i < lenB; i++) {
                lookupArray[i]['IsChecked'] = false;
                for (var j = 0; j < lenA; j++) {
                    if (selectedArray[j] === lookupArray[i][mapper.Id]) {
                        lookupArray[i]['IsChecked'] = true;
                        break;
                    }
                }
            }
        }
        return lookupArray;
    },

    getListWithIdsByCheckIsCheckedTrue: function (totalArray, mapper) {
        if (!mapper) {
            mapper = { Id: 'Id', Value: 'Value' };
        }

        var returnArrayOfIds = [];
        try {
            if (totalArray && totalArray.length > 0) {
                for (var i = 0; i < totalArray.length; i++) {
                    if (totalArray[i].IsChecked === true) {
                        returnArrayOfIds.push(totalArray[i][mapper.Id]);
                    }
                }
            }
        } catch (e) {

        }

        return returnArrayOfIds;
    },

    getListWithValueByCheckIsCheckedTrue: function (totalArray, mapper) {
        if (!mapper) {
            mapper = { Id: 'Id', Value: 'Value' };
        }
        var returnArrayValues = [];
        for (var i = 0; i < totalArray.length; i++) {
            if (totalArray[i].IsChecked === true) {
                returnArrayValues.push(totalArray[i][mapper.Id]);
            }

        }
        return returnArrayValues;
    },
    changeCoachPlayerIntoTeacherStudent: function () {

        try {
            var isCoach = localStorage._UserType === "Coach";
            var isPE = isCoach ? (localStorage._OrgSportsName === "physicaleducation" || localStorage._OrgSportsName === "arts" || localStorage._OrgSportsName === "education" || localStorage._OrgSportsName === "Education") : (localStorage['_SelectedSportsName'] === "Physical Education" || localStorage['_SelectedSportsName'] === "Arts" || localStorage['_SelectedSportsName'] === "Education");
            if (isPE) {
                $('.k-view-coach >a.k-link:contains("coach"),.k-view-coach >a.k-link:contains("Coach"),option.yes-teacher-student:contains("coach"),option.yes-teacher-student:contains("Coach"),span.k-input:contains("coach"),span.k-input:contains("Coach"),ul.k-list >li:contains("coach"),ul.k-list >li:contains("Coach"),.teamManagement > div > span:contains("coach"),.teamManagement > div > span:contains("Coach"),.popUpTitleBar > div > div:not(.not-teacher-student):contains("coach"),.popUpTitleBar > div > div:not(.not-teacher-student):contains("Coach"),.yes-teacher-student:contains("coach"),.yes-teacher-student:contains("Coach"),.alert p:not(.not-teacher-student):contains("Coach"),.alert p:not(.not-teacher-student):contains("coach"),.alert span:not(.not-teacher-student):contains("Coach"),.alert span:not(.not-teacher-student):contains("coach"),a:not(.dropdown-toggle):not(.not-teacher-student):contains("Coach"),a:not(.dropdown-toggle):not(.not-teacher-student):contains("coach"),label:not(.not-teacher-student):contains("Coach"),label:not(.not-teacher-student):contains("coach"),h1:not(.not-teacher-student):contains("Coach"),h1:not(.not-teacher-student):contains("coach"),.has-sub a span:not(.not-teacher-student):contains("coach"),.has-sub a span:not(.not-teacher-student):contains("Coach"),th:not(.not-teacher-student):contains("Coach"),th:not(.not-teacher-student):contains("coach")').each(function () {

                    var str = $(this).text();

                    str = str.replace(/\Coaches/g, 'Teachers');
                    str = str.replace(/\coaches/g, 'teachers');
                    str = str.replace(/\COACHES/g, 'TEACHERS');
                    str = str.replace(/\Coach/g, 'Teacher');
                    str = str.replace(/\coach/g, 'teacher');
                    str = str.replace(/\COACH/g, 'TEACHER');

                    str = str.replace('Coaches', 'Teachers');
                    str = str.replace('coaches', 'teachers');
                    str = str.replace('COACHES', 'TEACHERS');
                    str = str.replace('Coach', 'Teacher');
                    str = str.replace('coach', 'teacher');
                    str = str.replace('COACH', 'TEACHER');

                    $(this).text(str);
                });

                $('.k-view-coach >a.k-link:contains("player"),.k-view-coach >a.k-link:contains("Player"),option.yes-teacher-student:contains("player"),option.yes-teacher-student:contains("Player"),span.k-input:contains("player"),span.k-input:contains("Player"),ul.k-list >li:contains("player"),ul.k-list >li:contains("Player"),.teamManagement > div > span:contains("player"),.teamManagement > div > span:contains("Player"),.popUpTitleBar > div > div:not(.not-teacher-student):contains("player"),.popUpTitleBar > div > div:not(.not-teacher-student):contains("Player"),.yes-teacher-student:contains("player"),.yes-teacher-student:contains("Player"),.alert p:not(.not-teacher-student):contains("Player"),.alert p:not(.not-teacher-student):contains("player"),.alert p:not(.not-teacher-student):contains("players"),.alert span:not(.not-teacher-student):contains("Player"),.alert span:not(.not-teacher-student):contains("player"),a:not(.dropdown-toggle):not(.not-teacher-student):contains("Player"),a:not(.dropdown-toggle):not(.not-teacher-student):contains("player"),label:not(.not-teacher-student):contains("Player"),label:not(.not-teacher-student):contains("player"),h1:not(.not-teacher-student):contains("Player"),h1:not(.not-teacher-student):contains("player"),.has-sub a span:not(.not-teacher-student):contains("player"),.has-sub a span:not(.not-teacher-student):contains("Player"),th:not(.not-teacher-student):contains("Player"),th:not(.not-teacher-student):contains("player")').each(function () {
                    var str = $(this).text();
                    if (str.length > 0) {

                        str = str.replace(/Player/g, 'Student');
                        str = str.replace(/player/g, 'student');
                        str = str.replace(/PLAYER/g, 'STUDENT');

                        str = str.replace('Player', 'Student');
                        str = str.replace('player', 'student');
                        str = str.replace('PLAYER', 'STUDENT');

                        $(this).text(str);
                    }
                });

                $('input[type="text"]').each(function () {
                    try {
                        var str = $(this).attr('placeholder');

                        if (str.length > 0) {
                            str = str.replace(/\Coaches/g, 'Teachers');
                            str = str.replace(/\coaches/g, 'teachers');
                            str = str.replace(/\COACHES/g, 'TEACHERS');
                            str = str.replace(/\Coach/g, 'Teacher');
                            str = str.replace(/\coach/g, 'teacher');
                            str = str.replace(/\COACH/g, 'TEACHER');

                            str = str.replace('Coaches', 'Teachers');
                            str = str.replace('coaches', 'teachers');
                            str = str.replace('COACHES', 'TEACHERS');
                            str = str.replace('Coach', 'Teacher');
                            str = str.replace('coach', 'teacher');
                            str = str.replace('COACH', 'TEACHER');

                            str = str.replace(/Player/g, 'Student');
                            str = str.replace(/player/g, 'student');
                            str = str.replace(/PLAYER/g, 'STUDENT');

                            str = str.replace('Player', 'Student');
                            str = str.replace('player', 'student');
                            str = str.replace('PLAYER', 'STUDENT');

                            $(this).attr('placeholder', str);
                        }
                    } catch (e) {

                    }
                });
            }
        } catch (e) {

        }
    },
    isTabOrMobile: false,
    isMobile: false,
    getLargestIndexOfOpenedSchedulerPopup: function () {
        var openedPopup = $('.k-overlay + .k-widget.k-window').css('z-index');
        return openedPopup;
    },
    firstTriggeredSlideId: '',
    openedSliderQue: [],
    slideModalShow: function (popupId, offsetId, size, configs) {

        var configs = configs || { bgScroll: false };

        //console.log(configs);

        /* For preventing the body scroll while slider is on.*/
        if (this.firstTriggeredSlideId == '') {
            this.firstTriggeredSlideId = popupId;
            if (!configs.bgScroll) {
                $('body').toggleClass('affix');
            }
        } else if (this.firstTriggeredSlideId != '' && this.firstTriggeredSlideId == popupId) {
            this.firstTriggeredSlideId = '';
            if (!configs.bgScroll) {
                $('body').toggleClass('affix');
            }
        }


        /* Lets configure the sliders attributes.*/
        var popupSlide = popupId,
            popupSlideBody = popupId + 'Body',
            popupExpandId = popupId + 'CompExp  i',
            winHeight = $(window).height(),
            winWidth = $(window).width(),
            tabsOffset = $(offsetId).height(),
            height = winHeight - tabsOffset - 40;



        /* Lets check slider size.*/
        var slideWidth;

        //giving the slider full view while screen is less than 768px
        if ($(window).width() < 768) {
            size = 'F';
        }
        else {
            if (!size) {
                size = 'L';
            }
        }

        if (size == 'F') {
            slideWidth = '100%';
            tabsOffset = 0;
            height = winHeight;
        } else if (size == 'MF') { // for Medium width and Full height
            slideWidth = '75%';
            tabsOffset = 0;
            height = winHeight;
        } else if (size == 'L') {
            slideWidth = '95%';
        } else if (size == 'M') {
            slideWidth = '75%';
        } else if (size == 'S') {
            slideWidth = '50%';
        } else if (size == 'XS') {
            slideWidth = '30%';
        } else {
            slideWidth = '40%';
        }





        //if (size == 'F') {
        //    tabsOffset = 0;
        //    height = winHeight;
        //}

        /* Lets register the slider in Que.*/
        if ($.inArray(popupId, this.openedSliderQue) == -1) {
            this.openedSliderQue.push(popupId);
        } else {
            this.openedSliderQue = $.grep(this.openedSliderQue, function (value) {
                return value != popupId;
            });
        }
        var queIndex = $.inArray(popupId, this.openedSliderQue);



        /* Lets check if shceduler is present*/
        var scheduler_Z_Index;
        if (this.getLargestIndexOfOpenedSchedulerPopup() !== undefined) {
            scheduler_Z_Index = parseInt(this.getLargestIndexOfOpenedSchedulerPopup());
            //alert(scheduler_Z_Index);
        } else {
            scheduler_Z_Index = 10000;
            //alert('undefined');
        }



        /* Setting the slider's Z-Index. */
        if (queIndex < 1) {
            $(popupSlide).css({ 'top': tabsOffset, 'margin-left': 0, 'width': slideWidth, 'z-index': scheduler_Z_Index + 5 });
        }
        else {
            var prevOpenedSliderId = this.openedSliderQue[queIndex - 1];
            var zIndexVal = $(prevOpenedSliderId).css('z-index');
            zIndexVal = parseInt(zIndexVal) + 5;
            if (zIndexVal > scheduler_Z_Index) {
                $(popupSlide).css({ 'top': tabsOffset, 'margin-left': 0, 'width': slideWidth, 'z-index': zIndexVal });
            } else {
                $(popupSlide).css({ 'top': tabsOffset, 'margin-left': 0, 'width': slideWidth, 'z-index': scheduler_Z_Index + 5 });
            }
        }


        /* for slider overlay control */
        if (this.openedSliderQue.length == 0) {
            $('.slider-overlay').css({ 'display': 'none' });
        } else {
            $('.slider-overlay').css({ 'display': 'block' });
            if ($.inArray(popupId, this.openedSliderQue) == -1 || queIndex == 0) {
                var currZIndex = parseInt($(this.openedSliderQue[this.openedSliderQue.length - 1]).css('z-index'));
                $('.slider-overlay').css({ 'z-index': currZIndex - 1 });
            } else {
                $('.slider-overlay').css({ 'z-index': zIndexVal - 1 });
            }
        }


        $(popupSlideBody).css({ 'height': height, 'width': 100 + '%' });
        $(popupExpandId).addClass('fa-expand').removeClass(' fa-compress');
        $(popupSlide).animate({ width: "toggle" }, 250, function () { });
        $(popupSlide).attr('size', size);
        setTimeout(function () {
            global.changeCoachPlayerIntoTeacherStudent();
        }, 1000);


    },

    slideModalMinMax: function (popupId, offsetId, size, configs) {
        //console.log('ocuh');
        var popupSlide = popupId, popupSlideBody = popupId + 'Body',
            popupExpandId = popupId + 'CompExp  i',
            popupTitleBar = popupId + 'TitleBar',
            winHeight = $(window).height(),
            winWidth = $(window).width(),
            expandState = $(popupExpandId).hasClass('fa-expand');

        var slideWidth,
            queIndex = $.inArray(popupId, this.openedSliderQue);

        //giving the slider full view while screen is less than 768px
        if ($(window).width() < 768) {
            size = 'F';
        } else {
            if (!size) {
                size = $(popupSlide).attr('size');
            }
        }


        if (size == 'F') {
            slideWidth = '100%';
        } else if (size == 'MF') {
            slideWidth = '75%';
        } else if (size == 'L') {
            slideWidth = '95%';
        } else if (size == 'M') {
            slideWidth = '75%';
        } else if (size == 'S') {
            slideWidth = '50%';
        } else {
            slideWidth = '40%';
        }
        $(popupSlide).attr('size', size);
        if (expandState) {
            $(popupSlide).css({ 'top': 0, 'width': winWidth });
            $(popupSlideBody).css({ 'height': winHeight - 30, 'width': winWidth });
        } else {
            var tabsOffset = $(offsetId).height();
            var height = winHeight - $(offsetId).height() - 30;

            if (size == 'F' || size == 'MF') {
                tabsOffset = 0;
                height = winHeight;
            }
            $(popupSlide).css({ 'top': tabsOffset, 'margin-left': 0, 'width': slideWidth });
            $(popupSlideBody).css({ 'height': height, 'width': '100%' });
        }
        $(popupExpandId).toggleClass('fa-expand fa-compress');
    },

    sliderResize: function () {
        $(window).resize(function () {
            var winHeight = $(window).height(),
                winWidth = $(window).width(),
                slideWidth;

            $('.popupSlide').each(function (elem) {
                size = $(this).attr('size');
                if (size == 'F') {
                    slideWidth = '100%';
                } else if (size == 'MF') {
                    slideWidth = '75%';
                } else if (size == 'L') {
                    slideWidth = '95%';
                } else if (size == 'M') {
                    slideWidth = '75%';
                } else if (size == 'S') {
                    slideWidth = '50%';
                } else {
                    slideWidth = '40%';
                }
                if ($(this).find('.expandcompress i.fa').hasClass('fa-compress')) {
                    $(this).css({ 'width': winWidth, 'height': winHeight });
                    $(this).find('.popupSlideBody').css({ 'width': winWidth, 'height': winHeight });
                } else {
                    $(this).css({ 'width': slideWidth, 'height': winHeight });
                    $(this).find('.popupSlideBody').css({ 'height': winHeight, 'width': 100 + '%' });
                }
            });

        });
    },

    convertToInt: function (value) {
        try {
            return parseInt(value);
        }
        catch (err) {
            return 0;
        }
    },

    setSportsStorage: function (param) {
        var selectedSports = null;
        var sportsList = [];
        try {
            sportsList = JSON.parse(localStorage['_PlayerSports']);
            if (sportsList.length > 0) {
                selectedSports = global.getMatchedPropertyByProperty(sportsList, 'IsPrimary', true, null);
                if (selectedSports === null) {
                    selectedSports = sportsList[0];
                }
                localStorage['_SelectedSportsId'] = selectedSports.SportsId;
                localStorage['_SelectedSportsName'] = selectedSports.SportsName;
            }
        } catch (e) {
            console.log(e);
        }
    },

    validateQueryParameter: function (config) {
        if (config === null) {
            return false;
        }

        if (config.DataType === 'string') {
            return true;
        }
        else if (config.DataType === 'integer') {
            return true;
        }
        else {
            return false;
        }
    },

    getSoccerFormations: function () {
        var formations = [
            { Id: 'srf1', Value: '4-4-2', GK: [1], DF: [2, 3, 4, 5], MF: [6, 7, 8, 9], FW: [10, 11] },
            { Id: 'srf2', Value: '4-5-1', GK: [1], DF: [2, 3, 4, 5], MF: [6, 7, 8, 9, 10], FW: [11] },
            { Id: 'srf3', Value: '4-3-3', GK: [1], DF: [2, 3, 4, 5], MF: [6, 7, 8], FW: [9, 10, 11] },
            { Id: 'srf4', Value: '3-4-3', GK: [1], DF: [2, 3, 4], MF: [5, 6, 7, 8], FW: [9, 10, 11] },
            { Id: 'srf5', Value: '4-2-3-1', GK: [1], DF: [2, 3, 4, 5], MF: [6, 7, 8, 9, 10], FW: [11] },
            { Id: 'srf6', Value: '3-5-2', GK: [1], DF: [2, 3, 4], MF: [5, 6, 7, 8, 9], FW: [10, 11] },
            { Id: 'srf7', Value: '5-4-1', GK: [1], DF: [2, 3, 4, 5, 6], MF: [7, 8, 9, 10], FW: [11] }
        ];
        return formations;
    },

    getFormattedPhoneNumber: function (input) {
        if (input && !isNaN(input) && input.length === 10) {
            try {
                //xxx-xxx-xxxx
                return input.substring(0, 3) + "-" + input.substring(3, 6) + "-" + input.substring(6, 10);
            } catch (e) {
                return input;
            }
        }
        else {
            return input;
        }
    },

    removeSpecialCharsFromNumber: function (input) {
        try {
            var specialChars = ['-', '_', '.', '(', ')', '+'];
            for (var i = 0; i < specialChars.length; i++) {
                input = input.split(specialChars[i]).join('');
            }
        } catch (e) {

        }
        return input;
    },

    getOrganizationAppList: function () {

        var _orgId = 0;
        if (localStorage['_OrgId'] && localStorage['_OrgId'] !== '0') {
            _orgId = localStorage['_OrgId'];
        }
        else {
            return;
        }
        var getModel = {
            OrganizationId: _orgId,
            PageNumber: '',
            PageSize: '',
            ShowAll: ''
        };

        $.ajax({
            type: "GET",
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', 'bearer ' + localStorage._Token)
            },
            url: apiURL + "/organization/GetOrganizationAPP/" + _orgId,
            // data: getModel,
            complete: function (result, status) {
                $('#OrgAppId').html('');
                try {
                    if (result.responseText) {
                        var dataResult = JSON.parse(result.responseText);
                        var ListOfOrgAPP = dataResult.Data;
                        if (ListOfOrgAPP) {
                            for (var i = 0; i < ListOfOrgAPP.length; i++) {
                                $('#OrgAppId').append('<li><a href="' + ListOfOrgAPP[i].URL + '" target="_blank">' + ListOfOrgAPP[i].Name + '</a></li>');
                            }
                        }
                    }

                } catch (e) {
                    console.log(e);
                }
            }
        });

    },

    getCoachMyAppList: function () {
        var _userId = 0;
        if (localStorage['_UserId'] && localStorage['_UserId'] !== '0') {
            _userId = localStorage['_UserId'];
        }

        $.ajax({
            type: "GET",
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', 'bearer ' + localStorage._Token)
            },
            url: apiURL + "/organization/GetCoachAPP/" + _userId,
            complete: function (result, status) {
                $('#CoachMyAppId').html('');
                try {
                    var dataResult = JSON.parse(result.responseText);
                    var ListOfCoachMyAPP = dataResult.Data;
                    for (var i = 0; i < ListOfCoachMyAPP.length; i++) {
                        $('#CoachMyAppId').append('<li><a href="' + ListOfCoachMyAPP[i].URL + '" target="_blank">' + ListOfCoachMyAPP[i].Name + '</a></li>');
                    }

                } catch (e) {
                    console.log(e);
                }
            }
        });
    },

    getMillisecondQueryString: function () {
        return "?m=" + (new Date()).getMilliseconds();
    },

    rolesArrayToTree: function (nodes) {

        var idField = "OrganizationId", parentIdField = "ParentOrganizationId", finalSortBy = "OrganizationName", childField = "ChildOrg";

        //initially sort asc by parentid
        nodes.sort(function (a, b) {
            return a[parentIdField] - b[parentIdField];
        });

        var map = {}, node, roots = [];
        for (var i = 0; i < nodes.length; i += 1) {
            node = nodes[i];
            node[childField] = [];
            map[node[idField]] = i; // use map to look-up the parents
            if (node[parentIdField]) {
                try {
                    nodes[map[node[parentIdField]]][childField].push(node);
                } catch (e) {
                    //if parentorg not accessible/found keep it in root level
                    roots.push(node);
                }
            } else {
                roots.push(node);
            }
        }

        roots.sort(function (a, b) {
            return a[finalSortBy] - b[finalSortBy];
        });
        return roots;
    },

    getChildOrgListById: function (orgId, level) {
        var childOrgList = [];
        var rolelist = JSON.parse(localStorage._Roles).ROLELIST;
        var tempOrgList = null;
        var orgIds = [], tempOrgIds = [];
        var lvl = 1, defaultLevel = 5;
        orgIds.push(orgId);
        if (level === undefined || level === null) {
            level = defaultLevel;
        }

        while (lvl <= level) {
            tempOrgIds = [];
            for (var i = 0; i < orgIds.length; i++) {
                tempOrgList = global.getMatchedListByProperty(rolelist, "ParentOrganizationId", orgIds[i]);
                if (tempOrgList !== null) {
                    for (var j = 0; j < tempOrgList.length; j++) {
                        childOrgList.push({ OrganizationId: tempOrgList[j].OrganizationId, OrganizationName: tempOrgList[j].OrganizationName });
                        tempOrgIds.push(tempOrgList[j].OrganizationId);
                    }
                }
            }

            if (tempOrgIds) {
                orgIds = JSON.parse(JSON.stringify(tempOrgIds));
            }
            else {
                orgIds = []
            }
            lvl++;
        }

        return childOrgList;
    },

    getOrgMembershipEnableFlage: function () {
        var roleList = JSON.parse(localStorage._Roles).ROLELIST;
        var currentOrg = angular.copy(global.getMatchedPropertyByProperty(roleList, 'OrganizationId', parseInt(localStorage['_OrgId']), null));

        //console.log(JSON.stringify(currentOrg));

        if (currentOrg) {
            return currentOrg.EnableOrgMembershipFee;
        }
    },

    isOrgAdmin: function () {
        var returnVal = false;
        var roleList = JSON.parse(localStorage._Roles).ROLELIST;
        var currentOrg = global.getMatchedPropertyByProperty(roleList, 'OrganizationId', parseInt(localStorage['_OrgId']), null);
        // console.log(currentOrg);
        if (currentOrg) {
            if (currentOrg.RoleName == 'Org_Admin') {
                returnVal = true;
            } else {
                returnVal = false;
            }
        }
        return returnVal;
    },

    isOrgExecutive: function () {
        var returnVal = false;
        var roleList = JSON.parse(localStorage._Roles).ROLELIST;
        var currentOrg = global.getMatchedPropertyByProperty(roleList, 'OrganizationId', parseInt(localStorage['_OrgId']), null);
        // console.log(currentOrg);
        if (currentOrg) {
            if (currentOrg.RoleName == 'Org_Executive') {
                returnVal = true;
            } else {
                returnVal = false;
            }
        }
        return returnVal;
    },

    isShowTopTenUniversity: function () {
        var returnVal = false;
        if (localStorage._Roles) {
            var roleList = JSON.parse(localStorage._Roles).ROLELIST;
            var currentOrg = global.getMatchedPropertyByProperty(roleList, 'OrganizationId', parseInt(localStorage['_OrgId']));

            if (currentOrg) {
                if (currentOrg.OrgTypeLookupName === 'Professional' || currentOrg.OrgTypeLookupName === 'professional') {
                    returnVal = true;
                } else {
                    returnVal = false;
                }
            }
        }
        return returnVal;
    },

    //To check reservation enable flag
    getOrgReservationEnableFlag: function () {
        return $.ajax({
            url: apiURL + '/CoachProfile/GetRoles/' + localStorage["_OrgId"],
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', 'bearer ' + localStorage._Token);
            }
        });
    },

    //resert Organization Logo
    reSetOrgLogo: function () {
        if (localStorage._OrgLogo) {
            $("#dashLogo").attr("src", apiBaseURL + localStorage._OrgLogo + "?dt=" + (new Date).getTime().toString());
            $('.page-header .logo').css({ 'background': '#fff', 'border-bottom': '1px solid #fff' });
        }
    },

    customFormWizard: function (configs) {
        var defaultConfig = {
            capsuleClass: 'rwtn-capsule',
            capsuleIdPrefix: 'rwtTab',
            pannelClass: 'reg-wiz-tab-panel',
            pannelIdPrefix: 'rwtp',
            navIds: {
                prev: 'prv',
                next: 'nxt'
            }
        };
        configs = configs || defaultConfig;
        var pannelClass = '.' + configs.pannelClass,
            pannelIdPrefix = '#' + configs.pannelIdPrefix;
        var totalPanels = $(pannelClass).length,
            currentPanel = 1;
        // initialize capsule arrange
        var manageTopNavPosInit = manageTopNavPos(configs);
        setTimeout(function () {
            manageTopNavPosInit();
        }, 100);
        // initialize nav btn control
        showHideControls(currentPanel, totalPanels, configs);
        var changeCapsuleCall = new changeCapsule();
        changeCapsuleCall(currentPanel, configs);
        // to arrange top nav of the wizard
        function manageTopNavPos(configs) {
            return function () {
                var capsuleClass = '.' + configs.capsuleClass,
                    noOfRwtnCapsule = $(capsuleClass).length,
                    rwtnCapsuleSecWidth = 100 / noOfRwtnCapsule;
                $(capsuleClass).css({ 'width': rwtnCapsuleSecWidth - 1 + '%' });
            }
        }
        // to show hide control btn
        function showHideControls(currentPanelNo, totalPanelNo, config) {
            var noOfRwtnCapsule = totalPanelNo,
                navPrev = config.navIds.prev,
                navNext = config.navIds.next;
            if (currentPanelNo == 1) {
                $('#' + navPrev).css({
                    'display': 'none'
                });
            } else {
                $('#' + navPrev).css({
                    'display': 'inline-block'
                });
            }
            if (currentPanelNo == totalPanelNo) {
                $('#' + navNext).css({
                    'display': 'none'
                });
            } else {
                $('#' + navNext).css({
                    'display': 'inline-block'
                });
            }
        }
        // to change the wizard nav capsule
        function changeCapsule() {
            var calledCapsule = [];
            return function (currentPanelNo, configs) {
                var capsuleClass = '.' + configs.capsuleClass,
                    capsuleIdPrefix = '#' + configs.capsuleIdPrefix;
                var currentCapsule = capsuleIdPrefix + currentPanelNo;
                if ($.inArray(currentCapsule, calledCapsule) == -1) {
                    calledCapsule.push(currentCapsule);
                } else {
                    calledCapsule = $.grep(calledCapsule, function (value) {
                        return value != currentCapsule;
                    });
                }
                $(capsuleClass).removeClass('success-capsule');
                setTimeout(function () {
                    $.each(calledCapsule, function (k, v) {
                        var id = v;
                        $(id).addClass('success-capsule');
                        // console.log(id);
                    });
                }, 100);
            };
        }
        // to change the wizard panel
        return {
            nextPanel: function (skip) {
                var nextPage = skip ? (skip > 0 ? skip + 1 : 1) : 1;
                var currentPanelTemp = currentPanel;
                var targetPanel = currentPanel < totalPanels ? currentPanel + nextPage : currentPanel;
                currentPanel = currentPanel < totalPanels ? currentPanel + nextPage : currentPanel;
                var targetPanelId = pannelIdPrefix + targetPanel;

                $(pannelClass).fadeOut(400);
                setTimeout(function () {
                    $(targetPanelId).fadeIn('slow');
                }, 400);
                for (var i = 1; i <= nextPage; i++) {
                    changeCapsuleCall(currentPanelTemp + i, configs);
                }
                showHideControls(currentPanel, totalPanels, configs);
            },
            prevPanel: function (skip) {
                var prevPage = skip ? (skip > 0 ? skip + 1 : 1) : 1;
                for (var i = 0; i < prevPage; i++) {
                    changeCapsuleCall(currentPanel - i, configs);
                }
                var targetPanel = currentPanel > 1 ? currentPanel - prevPage : currentPanel;
                currentPanel = currentPanel > 1 ? currentPanel - prevPage : currentPanel;
                var targetPanelId = pannelIdPrefix + targetPanel;
                $(pannelClass).fadeOut(400);
                setTimeout(function () {
                    $(targetPanelId).fadeIn('slow');
                }, 400);
                showHideControls(currentPanel, totalPanels, configs);
            },
            getCurrentPanel: function () {
                return currentPanel;
            }
        }
    },

    //fixes for bootstrap modal overlay issue
    overlayFix: function (id) {
        if ($('body').find('.slider-overlay').css('display') == 'block') {
            var requiredZindex = parseInt($('body').find('.slider-overlay').css('z-index'));
            $(id).css({ 'z-index': requiredZindex + 5 });
            //console.log(requiredZindex);
        }
    },

    showEmailVerifyAlert: function () {
        /*if email is not verified*/
        if (localStorage['_IsEmailVerified'] === 'false') {
            $(document).ready(function () {

                $.ajax({
                    type: "GET",
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader('Authorization', 'bearer ' + localStorage["_Token"])
                    },
                    url: apiURL + "/UserInfo/CheckEmailVerified",
                    complete: function (result, status) {
                        //show dialog
                        if (result.responseText === "false") {
                            setTimeout(function () {
                                if (global.isRedirectedFromLogin) {
                                    $.get(apiURL + '/Values/GetCurrentDate').success(function (res) {
                                        var now = moment(res);
                                        var accDate = moment.utc(localStorage['_AccountCreatedDate']);
                                        var remDays = 30 - now.diff(accDate, 'd');
                                        var userEmail = localStorage['_UserEmail'];

                                        var msgHtml = '<p>Please check your email (' + userEmail + ') to verify it.</p>' +
                                            '<p>If you did not see an email from us in your inbox, then please check your spam folder</p>' +
                                            '<p><span class="hover-link" onclick="resendVerificationLink()">Click here to resend verification email</span></p>' +
                                            '<br/><p>Your account will be deactivated if not verified within ' + remDays + ' days.</p>';

                                        $("#email-verify-msg").html('').append(msgHtml);

                                        if (!isNaN(remDays)) {
                                            $("#email-verify-alert").modal('show');
                                        }
                                        global.isRedirectedFromLogin = false;
                                    });
                                }
                            }, 999);
                        }
                    }
                });

            });
        }
    },
    /*true in case of redirected from login page*/
    isRedirectedFromLogin: true,

    apiSuccessHandler: function (data, status, verb, inform, config, uri) {
        config = config || {};

        //if (config.HideSuccessMsgForMsgCodes)
        //{
        //    config.ShowSuccessMessage = false;
        //}

        //conditional hide msg rule for MsgCode cases
        if (config.HideSuccessMsgForMsgCodes && data.MsgCode && config.HideSuccessMsgForMsgCodes.indexOf(data.MsgCode) > -1) {
            config.ShowSuccessMessage = false;
        }

        if (inform.messages().length > 1) {
            inform.clear();
        }

        if (verb === 'post' && uri.indexOf('MarkInvitationAsRead') < 0) {
            if (data.WarningMessage) {
                var html = '';
                for (var i = 0; i < data.WarningMessage.length; i++) {
                    html += data.WarningMessage[i] + '<br/><br/>';
                }

                if (config.ShowSuccessMessage) {
                    inform.add(html, { "type": "warning", "ttl": 0, "html": true });
                }
            }
            else {
                if (config.ShowSuccessMessage) {
                    inform.add(config.DefaultSuccessMessage, { "type": "success", "ttl": 3000 });
                }
            }
        }
        else if (verb === 'delete') {
            if (config.ShowSuccessMessage) {
                inform.add(config.DefaultSuccessMessage, { "type": "success", "ttl": 3000 });
            }
        }
    },

    apiErrorHandler: function (data, status, inform, config) {
        config = config || {};

        if (status === 401) {
            $.get(apiURL + '/Values/GetCurrentDate').success(function (res) {

                if (moment(res) > moment(localStorage._TokenExpires)) {
                    if (global.requestRefreshToken) {
                        global.requestRefreshToken = false;
                        global.renewToken();
                    }
                }
                else {
                    //Redirect to Unauthorized page OR post unauthorized message
                    inform.add('Your session has expired. Please log in again.', { "type": "danger", "ttl": 3000 });
                    //logout
                }
            });
        }
        else if (status === 400) {
            if (typeof data === 'object' && data['IsError'] === true) {
                var html = '';
                for (var i = 0; i < data['Message'].length; i++) {
                    html += data['Message'][i].Value + '<br/>';
                }
                if (html === '') {
                    html = 'Sorry! an error occurred. Please try again later.';
                }
                inform.add(html, { "type": "danger", "ttl": config.InformTimeout, "html": true });
            }
            else if (typeof data === 'object' && data['Error'] === true) {
                inform.add(data['Message'], { "type": "danger", "ttl": config.InformTimeout, "html": true });
            }
            else {
                var error = global.parseError(data);
                var html = '';
                for (var i = 0; i < error.length; i++) {
                    html += error[i] + '<br>';
                }
                if (html === '') {
                    html = 'Sorry! an error occurred. Please try again later.';
                }
                inform.add(html, { "type": "danger", "ttl": config.InformTimeout, "html": true });
            }
        }
        else if (status === 500) {
            inform.add('Sorry! an error occurred. Please try again later.', { "type": "danger", "ttl": config.InformTimeout });
        }
    },

    findHighestZIndex: function (elem) {
        var divs = $(elem);
        var highest = 0;
        for (var i = 0; i < divs.length; i++) {
            var zindex = divs[i].style.zIndex;
            if (zindex > highest) {
                highest = zindex;
            }
        }
        return highest;
    },

    //compare arrays to check if same or not
    compareArrays: function (arr1, arr2) {
        // if the other array is a falsy value, return
        if (!arr1 || !arr2)
            return false;

        if (arr1.length === 0 && arr2.length === 0)
            return true;

        // compare lengths - can save a lot of time 
        if (arr1.length != arr2.length)
            return false;

        for (var i = 0, l = arr1.length; i < l; i++) {
            // Check if we have nested arrays
            if (arr1[i] instanceof Array && arr2[i] instanceof Array) {
                // recurse into the nested arrays
                if (!compareArrays(arr1[i], arr2[i]))
                    return false;
            }
            else {
                var exist = false;
                for (var j = 0; j < arr2.length; j++) {
                    if (arr1[i] == arr2[j]) {
                        exist = true;
                        break;
                    }
                }
                if (!exist)
                    return false;
            }
        }
        return true;
    },

    convertPackagesToString: function (packageJson) {
        try {
            //var packageJson = JSON.parse(localStorage._Packages);
            var packageArray = [];
            for (var i = 0; i < packageJson.length; i++) {
                packageArray.push(packageJson[i].PackageLookupName);
            }
            return packageArray.toString();
        } catch (e) {
            console.log(e);
            return "";
        }
    },

    noLicenseDialog: function (action) {
        try {
            var roleName = global.getMatchedPropertyByProperty(JSON.parse(localStorage._Roles).ROLELIST, 'OrganizationId', localStorage._OrgId, 'RoleName');
            if (roleName === "Org_Admin") {
                $(".show-if-admin").show();
                $(".hide-if-admin").hide();
            }
            else {
                $(".show-if-admin").hide();
                $(".hide-if-admin").show();
            }
        } catch (e) {

        }
        $("#noLicenseDialog").modal(action);
    },

    objectToQueryString: function (obj) {
        return Object.keys(obj).reduce(function (str, key, i) {
            var delimiter, val;
            delimiter = (i === 0) ? '?' : '&';
            key = encodeURIComponent(key);
            val = encodeURIComponent(obj[key]);
            return [str, delimiter, key, '=', val].join('');
        }, '');
    },

    hideProgramBuilder: function () {
        if (location.host === "ppnsports.com" && localStorage._OrgId === "28") {
            return true;
        }
        else {
            return false;
        }
    },

    hideMyTeam: function () {
        var isCoach = localStorage._UserType === "Coach";
        //var isPE = (localStorage._UserType === "Coach") ? (localStorage['_OrgSportsName'] === "physicaleducation") : (localStorage['_SelectedSportsName'] === "Physical Education");
        var isPE = isCoach ?
            (localStorage._OrgSportsName === "physicaleducation" || localStorage._OrgSportsName === "arts" || localStorage._OrgSportsName === "Education" || localStorage._OrgSportsName === "education")
            : (localStorage['_SelectedSportsName'] === "Physical Education" || localStorage['_SelectedSportsName'] === "Arts" || localStorage['_SelectedSportsName'] === "Education" || localStorage['_SelectedSportsName'] === "education");

        if (isPE) {
            $(".hide-my-team").hide();
        }
        else {
            $(".hide-my-team").show();
        }
    },

    hideParentMenu: function () {
        if (localStorage._UserType === "Parent") {


            var childList = [];
            try {
                childList = JSON.parse(localStorage['_ChildList']);
                if (childList && childList.length > 0) {
                    $("#parentMenuBtn").show();
                } else {
                    $("#parentMenuBtn").hide();
                }
            } catch (e) {
                $("#parentMenuBtn").hide();
            }
        }
    },

    sliderClose: function (sliderId, size) {
        if (!sliderId) return;
        size = size || "L";
        this.slideModalShow(sliderId, '.page-header', size);
        $(sliderId + '.popupSlide').removeAttr('size');
    },

    sliderMinMax: function (sliderId, size) {
        if (!sliderId) return;
        size = size || "L";
        this.slideModalMinMax(sliderId, '.page-header', size);
    },

    // common properties for rootScope
    rootScopeCommonDefaults: function (scope) {
        scope.rs_defaultUserImg = baseURL + "/Content/assets/images/ppn/user.jpg";
        scope.sliderClose = function (sliderId, size) {
            global.sliderClose(sliderId, size);
        }
        scope.sliderMinMax = function (sliderId, size) {
            global.sliderMinMax(sliderId, size);
        }
    },

    isNull: function (data, replaceBy) {
        if (data === null) {
            return replaceBy;
        }
        else {
            return data;
        }
    }
};


// Scheduler Slider Partial Control
TestSchedularSliderCloseBtn = function () {
    global.slideModalShow('#TestSchedularSlider', '.page-header', 'L');
};
TestSchedularSliderCompExp = function () {
    global.slideModalMinMax('#TestSchedularSlider', '.page-header', 'L');
}

function hideAlertMessage(id) {
    $(id).css("display", "none");
}

/*if season going to expire*/
try {
    if (localStorage['_OrgSeasonId'] && localStorage['_OrgSeasonId'] !== '0') {
        var seasons = global.getMatchedPropertyByProperty(JSON.parse(localStorage['_Roles']).ROLELIST, 'OrganizationId', localStorage['_OrgId'], 'OrgSeasons');
        var season = global.getMatchedPropertyByProperty(seasons, 'SeasonId', localStorage['_OrgSeasonId'], null);
        var abtToExpire = moment.utc(season['SeasonEndDate']) < moment.utc().add(16, 'days'); /*Check till 15 days ago if going to expire*/
        if (abtToExpire) {
            setTimeout(function () {
                $("#season-expire-msg").html('').append('"' + season["SeasonName"] + '"' + '<br/>This season is going to expire on ' + moment.utc(season["SeasonEndDate"]).format("MM/DD/YYYY"));
                $("#season-expire-alert").css("display", "block");
            }, 2000);
        }
        else {
            $("#season-expire-alert").css("display", "none");
        }
    }
    else {
        $("#season-expire-alert").css("display", "none");
    }
} catch (e) {
    console.log(e);
}


//Authorized ResendEmailConfirmation
function resendVerificationLink() {
    $.ajax({
        type: "GET",
        beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', 'bearer ' + localStorage["_Token"])
        },
        url: apiURL + "/Account/ResendEmailConfirmation",
        complete: function (result, status) {
            if (result.status === 200) {
                $("#email-verify-alert").modal('hide');
                alert("Please check your email. Thank you!");
            }
            else {
                alert("An error has occured. Please contact your administrator.");
            }
        }
    });
}

//setInterval(function () {
//    global.stateChangeDetect();
//}, 100);

//window.addEventListener("hashchange", function () {
//    console.log('back');
//    global.stateChangeDetect();
//});


//Check daylight savings
//Date.prototype.stdTimezoneOffset = function () {
//    var jan = new Date(this.getFullYear(), 0, 1);
//    var jul = new Date(this.getFullYear(), 6, 1);
//    return Math.max(jan.getTimezoneOffset(), jul.getTimezoneOffset());
//}

//Date.prototype.dst = function () {
//    return this.getTimezoneOffset() < this.stdTimezoneOffset();
//}