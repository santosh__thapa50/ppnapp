import { Component, OnInit } from '@angular/core';
import {
  FormGroup, Validators, FormBuilder, FormControl
} from '@angular/forms';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';


@Component({
  selector: 'app-form-example',
  templateUrl: './form-example.component.html',
  styleUrls: ['./form-example.component.css']
})
export class FormExampleComponent implements OnInit {
  model: any = {};
  registerForm: FormGroup;
  submitted = false;

  constructor(private http: HttpClient,
    private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      MemberShipFee: [null, Validators.required],
      DiscountAmount: [null, Validators.required],
      Remarks: [null, Validators.required],
      TotalAmount: [null]
    });
  }

  get f() { return this.registerForm.controls; }

  onBillSubmit() {
    const params = {
      OrganizationId: 10,
      OrgMembershipBillId: 15794,
      DiscountAmt: this.registerForm.value.DiscountAmount,
      Remarks: this.registerForm.value.Remarks
    };
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    } else {
      this.http.post<any>(`${environment.apiUrl}/api/OrgMembership/UpdateOrgMembershipBill`, params).subscribe(
        data => {
          console.log('POST Request is successful ', data);
        },
        error => {
          console.log('Error', error);
        }
      );
    }
  }

  onSubmit() {
    const params = {
      OrganizationId: 10,
      OrgMembershipBillId: 15794,
      DiscountAmt: this.model.DiscountAmount,
      Remarks: this.model.Remarks
    };
    this.http.post<any>(`${environment.apiUrl}/api/OrgMembership/UpdateOrgMembershipBill`, params).subscribe(
      data => {
        console.log('POST Request is successful ', data);
      },
      error => {
        console.log('Error', error);
      }
    );
  }

}
