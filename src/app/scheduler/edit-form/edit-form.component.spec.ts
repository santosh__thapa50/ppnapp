import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SchedulerEditFormComponent } from './edit-form.component';

describe('EditFormComponent', () => {
  let component: SchedulerEditFormComponent;
  let fixture: ComponentFixture<SchedulerEditFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SchedulerEditFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchedulerEditFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
