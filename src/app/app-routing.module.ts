import { NgModule } from '@angular/core';
import { CanActivate, Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login';
import { SchedulerComponent } from './scheduler';
import { AuthGuard } from './auth.guard';
import { FormExampleComponent } from './form-example/form-example.component';



const routes: Routes = [
  { path: '', component: LoginComponent },
  {
    path: 'scheduler', component: SchedulerComponent, canActivate:
      [AuthGuard]
  },
  {path : 'formExample' , component: FormExampleComponent }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard],
})
// export const routing = RouterModule.forRoot(routes);
export class AppRoutingModule { }
