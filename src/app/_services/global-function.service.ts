import { Injectable } from '@angular/core';
import * as $ from 'jquery';
@Injectable({
  providedIn: 'root'
})
@Injectable({
  providedIn: 'root'
})
export class GlobalFunctionService {

  constructor() { }

  /************Global functions (dependencies : jQuery)*************/
  public getMatchedListByProperty(arr, arrProp, matchValue) {
    var objList = $.grep(arr, function (o) {
      return o[arrProp] == matchValue;
    });
    if (objList != null && objList.length > 0) {
      return objList;
    } else {
      return null;
    }
  }

  public getMatchedPropertyByProperty(arr, arrProp, matchValue, retProp) {
    try {
      var obj = $.grep(arr, function (o) { return o[arrProp] == matchValue; });
      if (obj != null && obj.length > 0) {
        if (retProp !== undefined && retProp !== null) { return obj[0][retProp]; }
        else { return obj[0]; }
      } else { return null; }
    } catch (error) {
      console.log(error);
      //console.log(arguments.callee.caller);
      return null;
    }
  }
  public convertPackagesToString(packageJson) {
    try {
      //var packageJson = JSON.parse(localStorage._Packages);
      var packageArray = [];
      for (var i = 0; i < packageJson.length; i++) {
        packageArray.push(packageJson[i].PackageLookupName);
      }
      return packageArray.toString();
    } catch (e) {
      console.log(e);
      return "";
    }
}

}


