import { Injectable } from '@angular/core';
import {
  HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';



@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor {

  constructor() { }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const authToken = localStorage.getItem('token');
    if (authToken) {
      if (req.body == "[object FormData]") {
        let authReq = req.clone({
          headers: req.headers.set('Content-Type', null)
        });
        authReq = req.clone({
          headers: req.headers.append('Authorization', `Bearer ` + authToken)
        });
        return next.handle(authReq).pipe(
          map((event: HttpEvent<any>) => {
            if (event instanceof HttpResponse) {
              // console.log('event--->>>', event);
            }
            return event;
          }));
      } else {
        const authReq = req.clone({ setHeaders: { Authorization: `Bearer ` + authToken } });
        return next.handle(authReq);
      }
    }
    else {
      return next.handle(req);
    }
  }
}
