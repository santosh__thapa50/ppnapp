import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { GlobalFunctionService} from './global-function.service'

@Injectable({
  providedIn: 'root'
})
export class LicenseAndRoleService {

  constructor(
    private http: HttpClient,
    private global: GlobalFunctionService
  ) { 

  }
 public setUserSession(params){
    return $.post(`${environment.apiUrl}api/User/SetUserSession`, params);
  }
  public getOrgActiveLicense(orgId) {
    return this.http.get(`${environment.apiUrl}/api/License/OrganizationActiveLicense/` + orgId + "/true");
  }
  public getPlayerActiveLicense () {
    return this.http.get(`${environment.apiUrl}/api/License/PlayerLicenseGet`);
  }
  public hasRentalReservationPackage(psOrgId, redirectUrl) {
  var isCoachBasic = localStorage._LicenseType === 'coachbasic' ? true : false;
  var bool = false;
  if (!isCoachBasic && localStorage._Packages && localStorage._Packages.indexOf("rentalreservation") > -1) {
    if (psOrgId && psOrgId !== '0') {
      var org = this.global.getMatchedPropertyByProperty(JSON.parse(localStorage._Roles)["ROLELIST"], "OrganizationId", psOrgId, null);
      if (org) {
        bool = true;
      }
    }
    else {
      bool = true;
    }
  }

    if (bool) {
      window.open(redirectUrl, '_self');
    }
    else {
     // window.location = baseURL + '/admin-c#/errorReservation';
    }
  }

}
