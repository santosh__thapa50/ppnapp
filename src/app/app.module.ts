import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import 'core-js/es7/reflect'
import { HttpClientModule, HTTP_INTERCEPTORS ,HttpClientJsonpModule} from '@angular/common/http';
import { AlertService, AuthenticationService, TokenInterceptorService } from './_services';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login';
import { SchedulerComponent, SchedulerService,SchedulerEditFormComponent } from './scheduler';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SchedulerModule } from '@progress/kendo-angular-scheduler';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormExampleComponent } from './form-example/form-example.component';
import { EditorModule } from '@progress/kendo-angular-editor';
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { DateInputsModule } from '@progress/kendo-angular-dateinputs';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SchedulerComponent,
    SchedulerEditFormComponent,
    FormExampleComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    InputsModule,
    BrowserAnimationsModule,
    SchedulerModule,
    DropDownsModule,
    ReactiveFormsModule,
    HttpClientJsonpModule,
    ButtonsModule,
    DateInputsModule,
    EditorModule
  ],
  providers: [
    AlertService,
    AuthenticationService,
    SchedulerService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
